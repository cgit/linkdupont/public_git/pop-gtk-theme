%global		debug_package	%{nil}
%global		gittag0			2.2.3-0

Name:		pop-gtk-theme
Version:	2.2.3
Release:	1%{?dist}
Summary:	An adaptive Gtk+ theme

License:	GPLv2
URL:		https://github.com/system76/%{name}
Source0:	https://github.com/system76/%{name}/archive/%{gittag0}.tar.gz#/%{name}-%{version}.tar.gz

Requires:	gnome-themes-standard
Requires:	gtk-murrine-engine
BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	gdk-pixbuf2-devel
BuildRequires:	sassc
BuildRequires:	inkscape
BuildRequires:	librsvg2-devel
BuildRequires:	parallel
BuildRequires:	gnome-shell

BuildArch:	noarch


%description
%{summary}.


%prep
%autosetup -n gtk-theme-%{gittag0}

%build
make


%install
%make_install


%files
%license COPYING
%doc README.md
%{_datadir}/themes/*


%changelog
* Sun Nov 26 2017 Link Dupont <linkdupont@fedoraproject.org> - 2.2.3-1
- New upstream version

* Fri Aug 4 2017 Link Dupont <linkdupont@fedoraproject.org> - 2.0.1-1
- New upstream version

* Sat Jul 1 2017 Link Dupont <linkdupont@fedoraproject.org> - 1.3.1.13-1
- New upstream version

* Mon Jun 12 2017 Link Dupont <linkdupont@fedoraproject.org> - 1.3.1.2-1
- New upstream version
- New versioning scheme
* Mon May 8 2017 Link Dupont <linkdupont@fedoraproject.org> - 3.90.0.81-6e16e77.1
- Initial package
